### Revolut Test Payments

The application allows you to create accounts with initial balance and make transfers between different accounts.

#### Techologies
- Java 11
- Jersey - REST framework
- Jetty - HTTP server
- JUnit/Mockito/JerseyTest - Test frameworks
- Lombok - boilerplate generator
- Gradle - project build

#### Run the application

The property "server.port" defines the server port. Default value is 80. The command to run:

```
gradle run
```

#### Create an account

Request:

```
POST http://localhost:80/account/
{"balance" : 100.1}
```

Response:
```
201 Created

{
  "balance": 100.1,
  "id": 1
}
```

#### Transfer

Request:

```
POST http://localhost:80/account/transfer

{"from" : 1, "to" : 2, "amount" : 40}
```

Response:
```
200 OK
```