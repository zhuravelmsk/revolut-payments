package zhuravel.revolut.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import zhuravel.revolut.api.dto.AccountDTO;
import zhuravel.revolut.api.dto.TransferDTO;
import zhuravel.revolut.api.impl.AccountRemoteServiceImpl;
import zhuravel.revolut.entity.Account;
import zhuravel.revolut.repository.AccountRepository;
import zhuravel.revolut.service.AccountService;
import zhuravel.revolut.service.PaymentService;
import zhuravel.revolut.service.impl.AccountServiceImpl;
import zhuravel.revolut.service.impl.PaymentServiceImpl;

import static org.junit.Assert.assertEquals;

public class AccountRemoteServiceTest extends JerseyTest {
    private final static List<Account> testAccounts = List.of(
            new Account().setId(1L).setBalance(new BigDecimal(100)),
            new Account().setId(2L).setBalance(new BigDecimal(50)),
            new Account().setId(3L).setBalance(new BigDecimal(100)),
            new Account().setId(4L).setBalance(new BigDecimal(50))
    );

    @Override
    protected Application configure() {
        AccountRepository accountRepository = new AccountRepository();
        AccountService accountService = new AccountServiceImpl(accountRepository);
        PaymentService paymentService = new PaymentServiceImpl(accountService);
        AccountRemoteService accountRemoteService = new AccountRemoteServiceImpl(accountService, paymentService);

        testAccounts.forEach(accountRepository::saveOrUpdate);

        return new ResourceConfig().register(accountRemoteService);
    }

    @Test
    public void get_validId_thenOk() {
        Response response = target("account/1").request().get();

        assertEquals("Response should be 200 OK", Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Account should be correct", response.readEntity(AccountDTO.class), AccountDTO.fromAccount(testAccounts.get(0)));
    }

    @Test
    public void get_invalidId_then404Error() {
        Response response = target("account/11").request().get();

        assertEquals("Response should be 404 Not Found", Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void create_validBalance_thenCreated201() {
        AccountDTO valid = new AccountDTO().setBalance(new BigDecimal(100));
        Response response = target("account").request().post(Entity.json(valid));

        assertEquals("Response should be 201 Created", Response.Status.CREATED.getStatusCode(), response.getStatus());
    }

    @Test
    public void create_invalidBalance_then400Error() {
        AccountDTO invalid = new AccountDTO().setBalance(new BigDecimal(-1));
        Response response = target("account").request().post(Entity.json(invalid));

        assertEquals("Response should be 400 Bad request", Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void create_noBody_then400Error()  {
        Response response = target("account").request().post(null);

        assertEquals("Response should be 400 Created", Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void transfer_invalidAccountId_then422Error() {
        TransferDTO invalidTransfer = new TransferDTO().setFrom(1).setTo(999).setAmount(new BigDecimal(10));
        Response response = target("account/transfer").request().post(Entity.json(invalidTransfer));

        assertEquals("Response should be 422 Unprocessable Entity", 422, response.getStatus());
    }

    @Test
    public void transfer_invalidAmount_then400Error() {
        TransferDTO invalidTransfer = new TransferDTO().setFrom(1).setTo(2).setAmount(new BigDecimal(-1));
        Response response = target("account/transfer").request().post(Entity.json(invalidTransfer));

        assertEquals("Response should be 400 Bad request", Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void transfer_insufficientBalance_then422Error() {
        TransferDTO invalidTransfer = new TransferDTO().setFrom(1).setTo(2).setAmount(new BigDecimal(101));
        Response response = target("account/transfer").request().post(Entity.json(invalidTransfer));

        assertEquals("Response should be 422 Unprocessable Entity", 422, response.getStatus());
    }

    @Test
    public void transfer_sameAccounts_then400Error() {
        TransferDTO invalidTransfer = new TransferDTO().setFrom(1).setTo(1).setAmount(new BigDecimal(10));
        Response response = target("account/transfer").request().post(Entity.json(invalidTransfer));

        assertEquals("Response should be 400 Bad request", Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void transfer_valid_thenCompleted() {
        TransferDTO invalidTransfer = new TransferDTO().setFrom(1).setTo(2).setAmount(new BigDecimal(10));

        Response response = target("account/transfer").request().post(Entity.json(invalidTransfer));
        assertEquals("Response should be 200 OK", Response.Status.OK.getStatusCode(), response.getStatus());

        response = target("account/1").request().get();
        assertEquals("Response should be 200 OK", Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Balance should be 90", response.readEntity(AccountDTO.class).getBalance(), new BigDecimal(90));

        response = target("account/2").request().get();
        assertEquals("Response should be 200 OK", Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Balance should be 60", response.readEntity(AccountDTO.class).getBalance(), new BigDecimal(60));
    }

    @Test
    public void transferConcurrent_valid_thenCompleted() {
        ExecutorService executorService = Executors.newFixedThreadPool(5);

        CompletableFuture[] tasks = IntStream
                .range(0, 5)
                .mapToObj(i -> getRunnableTransfer(3L, 4L, 1,20))
                .map(runnable -> CompletableFuture.runAsync(runnable, executorService))
                .toArray(CompletableFuture[]::new);

        CompletableFuture.allOf(tasks).join();

        Response response;

        response = target("account/3").request().get();
        assertEquals("Response should be 200 OK", Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Balance should be 0", response.readEntity(AccountDTO.class).getBalance(), BigDecimal.ZERO);

        response = target("account/4").request().get();
        assertEquals("Response should be 200 OK", Response.Status.OK.getStatusCode(), response.getStatus());
        assertEquals("Balance should be 150", response.readEntity(AccountDTO.class).getBalance(), new BigDecimal(150));
    }

    private Runnable getRunnableTransfer(long from, long to, int amount, int count) {
        return () -> {
            for (int i = 0; i < count; i++) {
                TransferDTO transferDTO = new TransferDTO().setFrom(from).setTo(to).setAmount(new BigDecimal(amount));
                target("account/transfer").request().post(Entity.json(transferDTO));
            }
        };
    }
}