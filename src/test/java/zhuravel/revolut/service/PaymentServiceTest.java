package zhuravel.revolut.service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import zhuravel.revolut.entity.Account;
import zhuravel.revolut.exception.AccountNotFoundException;
import zhuravel.revolut.exception.InsufficientBalanceException;
import zhuravel.revolut.exception.InvalidAccountException;
import zhuravel.revolut.exception.InvalidAmountException;
import zhuravel.revolut.exception.TransferSameAccountException;
import zhuravel.revolut.repository.AccountRepository;
import zhuravel.revolut.service.impl.AccountServiceImpl;
import zhuravel.revolut.service.impl.PaymentServiceImpl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PaymentServiceTest {
    private static AccountRepository accountRepository;
    private static PaymentService paymentService;

    private Map<Long, Account> testAccounts;

    @BeforeClass
    public static void setUp() {
        accountRepository = mock(AccountRepository.class);
        paymentService = new PaymentServiceImpl(new AccountServiceImpl(accountRepository));
    }

    @Before
    public void before() {
        testAccounts = Map.of(
                1L, new Account().setId(1L).setBalance(new BigDecimal(100)),
                2L, new Account().setId(2L).setBalance(new BigDecimal(100))
                );

        when(accountRepository.get(1L)).thenReturn(Optional.of(testAccounts.get(1L)));
        when(accountRepository.get(2L)).thenReturn(Optional.of(testAccounts.get(2L)));
    }

    @Test
    public void transfer_valid_thenOk() {
        paymentService.transfer(1L, 2L, new BigDecimal(10));

        verify(accountRepository, times(2)).saveOrUpdate(any());

        Assert.assertEquals(testAccounts.get(1L).getBalance(), new BigDecimal(90));
        Assert.assertEquals(testAccounts.get(2L).getBalance(), new BigDecimal(110));
    }

    @Test(expected = InsufficientBalanceException.class)
    public void transfer_insufficientBalance_thenException() {
        paymentService.transfer(1L, 2L, new BigDecimal(101));
    }

    @Test(expected = AccountNotFoundException.class)
    public void transfer_notFoundAccount_thenException() {
        paymentService.transfer(1L, 999L, new BigDecimal(10));
    }

    @Test(expected = InvalidAccountException.class)
    public void transfer_invalidAccount_thenException() {
        paymentService.transfer(1L, -1L, new BigDecimal(10));
    }

    @Test(expected = TransferSameAccountException.class)
    public void transfer_sameAccount_thenException() {
        paymentService.transfer(1L, 1L, new BigDecimal(10));
    }

    @Test(expected = InvalidAmountException.class)
    public void transfer_invalidAmount_thenException() {
        paymentService.transfer(1L, 1L, new BigDecimal(-10));
    }
}