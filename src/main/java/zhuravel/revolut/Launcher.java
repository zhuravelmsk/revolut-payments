package zhuravel.revolut;

import zhuravel.revolut.api.AccountRemoteService;
import zhuravel.revolut.api.impl.AccountRemoteServiceImpl;
import zhuravel.revolut.repository.AccountRepository;
import zhuravel.revolut.service.AccountService;
import zhuravel.revolut.service.PaymentService;
import zhuravel.revolut.service.impl.AccountServiceImpl;
import zhuravel.revolut.service.impl.PaymentServiceImpl;

public class Launcher {
    public static void main(String[] args) throws Exception {
        AccountRemoteService accountRemoteService = buildAccountRemoteService();
        JettyUtils.runServer(accountRemoteService);
    }

    private static AccountRemoteService buildAccountRemoteService() {
        AccountRepository accountRepository = new AccountRepository();
        AccountService accountService = new AccountServiceImpl(accountRepository);
        PaymentService paymentService = new PaymentServiceImpl(accountService);
        return new AccountRemoteServiceImpl(accountService, paymentService);
    }
}
