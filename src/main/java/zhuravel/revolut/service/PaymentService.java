package zhuravel.revolut.service;

import java.math.BigDecimal;

import zhuravel.revolut.exception.InsufficientBalanceException;
import zhuravel.revolut.exception.TransferSameAccountException;

public interface PaymentService {
    void transfer(long accountIdFrom, long accountIdTo, BigDecimal amount) throws InsufficientBalanceException, TransferSameAccountException;
}
