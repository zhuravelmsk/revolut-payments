package zhuravel.revolut.service.impl;

import java.math.BigDecimal;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;

import zhuravel.revolut.entity.Account;
import zhuravel.revolut.exception.AccountNotFoundException;
import zhuravel.revolut.exception.InvalidBalanceException;
import zhuravel.revolut.repository.AccountRepository;
import zhuravel.revolut.service.AccountService;

public class AccountServiceImpl implements AccountService {
    private final AccountRepository repository;

    public AccountServiceImpl(AccountRepository repository) {
        this.repository = repository;
    }

    @Override
    public Account findById(long id) {
        return repository.get(id).orElseThrow(() -> new AccountNotFoundException("Account doesn't exist"));
    }

    @Override
    public void saveOrUpdate(Account account) {
        validate(account);
        repository.saveOrUpdate(account);
    }

    @Override
    public void executeInLock(Long id1, Long id2, BiConsumer<Account, Account> operation) {
        ReentrantLock lock1 = findById(id1).getLock();
        ReentrantLock lock2 = findById(id2).getLock();

        Runnable runOperation = () -> operation.accept(findById(id1), findById(id2));

        if (id1 > id2) {
            executeInLockOrdered(lock1, lock2, runOperation);
        } else {
            executeInLockOrdered(lock2, lock1, runOperation);
        }
    }

    private void executeInLockOrdered(ReentrantLock lock1, ReentrantLock lock2, Runnable operation) {
        lock1.lock();
        try {
            lock2.lock();
            try {
                operation.run();
            } finally {
                lock2.unlock();
            }
        } finally {
            lock1.unlock();
        }
    }

    private void validate(Account account) {
        BigDecimal balance = account.getBalance();
        if (balance == null || BigDecimal.ZERO.compareTo(balance) > 0) {
            throw new InvalidBalanceException("Balance should be greater than 0");
        }
    }
}
