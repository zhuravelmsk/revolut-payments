package zhuravel.revolut.repository;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

import zhuravel.revolut.entity.Account;

public class AccountRepository {
    private final AtomicLong COUNTER = new AtomicLong(1);
    private final Map<Long, Account> accounts = new ConcurrentHashMap<>();

    public Optional<Account> get(long id) {
        return Optional.ofNullable(accounts.get(id));
    }

    public void saveOrUpdate(Account account) {
        if (account.getId() == null) {
            account.setId(COUNTER.getAndIncrement());
        }
        accounts.put(account.getId(), account);
    }
}
